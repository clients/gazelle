#!/bin/bash

git fetch origin master

version_local=$(grep 'version:' pubspec.yaml | sed 's/version: //')
if [ -z "$version_local" ]; then
    echo "Error: Version number not found in local pubspec.yaml."
    exit 1
fi

version_remote=$(git show origin/master:pubspec.yaml | grep 'version:' | sed 's/version: //')
if [ -z "$version_remote" ]; then
    echo "Error: Version number not found in remote pubspec.yaml."
    exit 1
fi

if [ "$version_local" != "$version_remote" ]; then
    echo "Error: Local and remote version numbers do not match."
    exit 1
fi

git fetch --tags
if git rev-parse "$version_local" >/dev/null 2>&1; then
    echo "Error: Git tag $version_local already exists."
    exit 1
fi

echo "Version number found: $version_local"

if ! git tag $version_local; then
    echo "Error: Git tag creation failed."
    exit 1
fi

if ! git push origin $version_local; then
    echo "Error: Git tag push failed."
    exit 1
fi

echo "Git tag $version_local has been successfully pushed."
