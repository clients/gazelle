import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/global.dart';
import 'package:gazelle/services/app_router.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:gazelle/providers/theme_provider.dart';
import 'package:path/path.dart' as path;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Initialize the app directory
  final appDocDir = await getApplicationDocumentsDirectory();
  appDir = Directory(path.join(appDocDir.path, 'gazelle'));
  if (!await appDir.exists()) {
    await appDir.create(recursive: true);
  }

  // Initialize Hive
  Hive.init(appDir.path);
  await Hive.openBox('settings');

  runApp(const ProviderScope(child: MainApp()));
}

class MainApp extends ConsumerWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isDarkMode = ref.watch(themeProvider);
    return MaterialApp.router(
      routerConfig: appRouter,
      title: 'Ğazelle',
      theme: isDarkMode ? ThemeData.dark() : ThemeData.light(),
    );
  }
}
