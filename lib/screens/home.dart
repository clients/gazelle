import 'package:durt2/durt2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/global.dart';
import 'package:gazelle/services/home_initializer.dart';
import 'package:gazelle/screens/home_section.dart';
import 'package:gazelle/screens/widgets/wallet_actionbar.dart';
import 'package:go_router/go_router.dart';

class HomeScreen extends ConsumerStatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {
  late Future<void> homeInitializer;

  @override
  void initState() {
    homeContext = context;
    globalRef = ref;
    homeInitializer = HomeInitializer(ref).init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ğazelle'),
        leading: IconButton(
          icon: const Icon(Icons.settings),
          onPressed: () {
            context.push('/settings');
          },
        ),
      ),
      body: FutureBuilder(
        future: homeInitializer,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            log.e('Error: ${snapshot.error}');
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          } else if (snapshot.connectionState != ConnectionState.done) {
            return const Center(child: CircularProgressIndicator());
          } else {
            return Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(8),
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Node: ${Durt.instance.endpoint}',
                    style: const TextStyle(fontSize: 12),
                  ),
                ),
                const Divider(),
                Expanded(
                  child: Center(
                    child: HomeSection(),
                  ),
                ),
                const Divider(),
                WalletActionBar(),
              ],
            );
          }
        },
      ),
    );
  }
}
