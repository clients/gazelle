import 'package:durt2/durt2.dart' show WalletService;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/providers/wallet_provider.dart';
import 'package:go_router/go_router.dart';
import 'package:gazelle/global.dart';

class PinCodeDialog extends ConsumerStatefulWidget {
  const PinCodeDialog({super.key});

  @override
  PinCodeDialogState createState() => PinCodeDialogState();
}

class PinCodeDialogState extends ConsumerState<PinCodeDialog> {
  final pinCodeController = TextEditingController();
  bool? isPinValid;
  bool isLoading = false;

  void onPinCodeSubmitted(String value) async {
    if (value.length != 4) {
      return;
    }
    setState(() {
      isLoading = true;
    });
    final walletData = ref.read(walletManagerProvider);
    final mnemonicLoaded = await WalletService.retriveMnemonicIsolate(
        walletData!.safeBoxNumber, value);

    setState(() {
      isLoading = false;
      isPinValid = mnemonicLoaded != null;
      if (isPinValid!) {
        homeContext.pop(mnemonicLoaded);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Enter PIN Code'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            controller: pinCodeController,
            autofocus: true,
            keyboardType: TextInputType.number,
            obscureText: true,
            maxLength: 4,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            onSubmitted: (value) {
              onPinCodeSubmitted(value);
            },
            decoration: const InputDecoration(
              labelText: 'PIN Code',
              border: OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 10),
          AnimatedOpacity(
            duration: const Duration(milliseconds: 200),
            opacity: isPinValid == false ? 1 : 0,
            child: const Text(
              'Invalid PIN Code',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
      actions: [
        SizedBox(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: ElevatedButton(
              onPressed: isLoading
                  ? null
                  : () {
                      onPinCodeSubmitted(pinCodeController.text);
                    },
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
              child: isLoading
                  ? const SizedBox(
                      width: 24,
                      height: 24,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                        strokeWidth: 2,
                      ),
                    )
                  : const Text('OK'),
            ),
          ),
        ),
      ],
    );
  }
}

Future<String?> showPinCodeDialog() async {
  return showDialog<String>(
    context: homeContext,
    builder: (BuildContext context) {
      return const PinCodeDialog();
    },
  );
}
