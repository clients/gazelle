import 'package:durt2/durt2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/providers/transaction_provider.dart';

class TransactionStatusWidget extends ConsumerStatefulWidget {
  const TransactionStatusWidget({super.key});

  @override
  ConsumerState<TransactionStatusWidget> createState() =>
      _TransactionStatusWidgetState();
}

class _TransactionStatusWidgetState
    extends ConsumerState<TransactionStatusWidget> {
  @override
  void dispose() {
    super.dispose();
  }

  Widget _transactionStatusContent(TransactionStatusModel? status) {
    if (status == null) {
      return const SizedBox.shrink();
    }

    switch (status.state) {
      case TransactionState.pending:
        return _buildRow(
          const CircularProgressIndicator(strokeWidth: 2),
          'Transaction in progress...',
        );
      case TransactionState.inBlock:
        return _buildRow(
          const Icon(Icons.check_circle_outline, color: Colors.green),
          'Transaction in a block',
        );
      case TransactionState.finalized:
        return _buildRow(
          const Icon(Icons.check_circle, color: Colors.green),
          'Transaction finalized',
        );
      case TransactionState.error:
        return _buildRow(
          const Icon(Icons.error, color: Colors.red),
          status.errorMessage ?? 'Transaction error',
        );
      default:
        return const SizedBox.shrink();
    }
  }

  Widget _buildRow(Widget leading, String text) {
    return IntrinsicWidth(
      child: Row(
        children: [
          SizedBox(
            width: 24,
            height: 24,
            child: leading,
          ),
          const SizedBox(width: 8),
          Text(text),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final transactionStatus = ref.watch(transactionManagerProvider);
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 500),
      transitionBuilder: (child, animation) => FadeTransition(
        opacity: animation,
        child: child,
      ),
      child: _transactionStatusContent(transactionStatus),
    );
  }
}
