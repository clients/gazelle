import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/providers/blockchain_listener.dart';
import 'package:gazelle/providers/wallet_provider.dart';
import 'package:gazelle/screens/widgets/custom_button.dart';
import 'package:gazelle/screens/widgets/identity_name.dart';
import 'package:gazelle/screens/widgets/transaction_status.dart';
import 'package:gazelle/screens/transfer_dialog.dart';
import 'package:gazelle/utils.dart';

class WalletActionBar extends ConsumerWidget {
  const WalletActionBar({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final walletInfo = ref.watch(walletManagerProvider);

    if (walletInfo == null) {
      return SizedBox(
        height: 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'No wallet',
              style: Theme.of(context).textTheme.bodyLarge!,
            ),
          ],
        ),
      );
    }

    return SizedBox(
      width: double.infinity,
      child: LayoutBuilder(builder: (context, constraints) {
        final isSmallScreen = constraints.maxWidth < 600;
        return Padding(
          padding:
              const EdgeInsets.only(left: 21, right: 21, bottom: 26, top: 18),
          child: Wrap(
            alignment: WrapAlignment.values[isSmallScreen ? 4 : 3],
            crossAxisAlignment: WrapCrossAlignment.center,
            spacing: 16,
            runSpacing: 16,
            children: [
              Wrap(
                spacing: 16,
                crossAxisAlignment: WrapCrossAlignment.center,
                alignment: WrapAlignment.center,
                children: [
                  Container(
                    width: 52,
                    height: 52,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage('assets/gazelle-72.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IdentityName(
                        address: walletInfo.address,
                        style: Theme.of(context).textTheme.titleLarge!,
                      ),
                      const SizedBox(height: 4),
                      GestureDetector(
                        onTap: () {
                          Clipboard.setData(
                              ClipboardData(text: walletInfo.address));
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text('Address copied to clipboard'),
                            ),
                          );
                        },
                        child: Text(
                          getShortAddress(walletInfo.address),
                          style:
                              Theme.of(context).textTheme.bodyMedium!.copyWith(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onSurface
                                        .withOpacity(0.7),
                                  ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Wrap(
                spacing: 16,
                runSpacing: 16,
                crossAxisAlignment: WrapCrossAlignment.center,
                alignment: WrapAlignment.center,
                children: [
                  const TransactionStatusWidget(),
                  Consumer(
                    builder: (context, ref, _) {
                      final blockchainState = ref.watch(
                          blockchainListenerProvider(walletInfo.address));
                      return Text(
                        '${blockchainState.balance} ĞDev',
                        style: Theme.of(context).textTheme.headlineSmall,
                      );
                    },
                  ),
                  const SizedBox(width: 0),
                  CustomButton(
                    text: 'Transfer',
                    icon: Icons.send,
                    onPressed: () => showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return TransferDialog();
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      }),
    );
  }
}
