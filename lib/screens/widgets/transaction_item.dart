import 'package:durt2/durt2.dart'
    show Query$GetAccountHistory$transferConnection$edges$node;
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/providers/wallet_provider.dart';
import 'package:gazelle/utils.dart';
import 'package:intl/intl.dart';

class TransactionItem extends ConsumerWidget {
  final Query$GetAccountHistory$transferConnection$edges$node transaction;

  const TransactionItem(this.transaction);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final walletAddress = ref.read(walletManagerProvider)!.address;
    final isOutgoing = transaction.fromId == walletAddress;
    final amountFormatted = transaction.amount / 100;

    return ListTile(
      leading: isOutgoing
          ? const Icon(Icons.call_made)
          : const Icon(Icons.call_received),
      title: isOutgoing
          ? Text('To ${getShortAddress(transaction.toId!)}')
          : Text('From ${getShortAddress(transaction.fromId!)}'),
      subtitle: Text(DateFormat.yMd().add_Hms().format(transaction.timestamp)),
      trailing: Text(
        '${isOutgoing ? '-' : '+'} $amountFormatted ĞDev',
        style: TextStyle(
          color: isOutgoing ? Colors.blue : Colors.green,
          fontSize: 14,
        ),
      ),
    );
  }
}
