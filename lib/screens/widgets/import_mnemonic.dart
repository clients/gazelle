import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/providers/wallet_provider.dart';
import 'package:gazelle/screens/widgets/new_pin_code_dialog.dart';
import 'package:gazelle/services/wallet_service.dart';

class ImportMnemonic extends ConsumerWidget {
  const ImportMnemonic({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final walletManager = ref.read(walletManagerProvider.notifier);
    final currentMnemonic = TextEditingController();

    void onMnemonicSubmitted() async {
      final pinCode = await showNewPinCodeDialog();
      if (pinCode != null) {
        await WalletService.generateWallet(
          mnemonic: currentMnemonic.text,
          pinCode: pinCode,
        );
        currentMnemonic.clear();
      }
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            'Import your wallet',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 16),
          TextField(
            controller: currentMnemonic,
            autofocus: true,
            onChanged: (value) {
              walletManager.isMnemonicValid.value = value.isNotEmpty;
            },
            onSubmitted: (value) async {
              if (walletManager.isMnemonicValid.value) {
                onMnemonicSubmitted();
              }
            },
            decoration: const InputDecoration(
              labelText: 'Mnemonic',
              border: OutlineInputBorder(),
            ),
            maxLines: 1,
          ),
          const SizedBox(height: 16),
          ValueListenableBuilder<bool>(
            valueListenable: walletManager.isMnemonicValid,
            builder: (context, isMnemonicValid, _) {
              return ElevatedButton(
                onPressed: isMnemonicValid
                    ? () async {
                        onMnemonicSubmitted();
                      }
                    : null,
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size(double.infinity, 48),
                ),
                child: const Text('Import'),
              );
            },
          ),
        ],
      ),
    );
  }
}
