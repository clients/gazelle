// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gazelle/global.dart';
import 'package:go_router/go_router.dart';

class NewPinCodeDialog extends StatefulWidget {
  const NewPinCodeDialog({super.key});

  @override
  NewPinCodeDialogState createState() => NewPinCodeDialogState();
}

class NewPinCodeDialogState extends State<NewPinCodeDialog> {
  final pinCodeController = TextEditingController();

  void onPinCodeSubmitted(String value) async {
    if (value.length != 4) {
      return;
    }
    final pinCode = int.parse(value);
    homeContext.pop(pinCode);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Set a new PIN code'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            controller: pinCodeController,
            autofocus: true,
            keyboardType: TextInputType.number,
            obscureText: true,
            maxLength: 4,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            onSubmitted: (value) {
              onPinCodeSubmitted(value);
            },
            decoration: const InputDecoration(
              labelText: 'New PIN code',
              border: OutlineInputBorder(),
            ),
          ),
        ],
      ),
      actions: [
        SizedBox(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: ElevatedButton(
              onPressed: () {
                onPinCodeSubmitted(pinCodeController.text);
              },
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
              child: const Text('Set'),
            ),
          ),
        ),
      ],
    );
  }
}

Future<int?> showNewPinCodeDialog() async {
  return showDialog<int>(
    context: homeContext,
    builder: (BuildContext context) {
      return const NewPinCodeDialog();
    },
  );
}
