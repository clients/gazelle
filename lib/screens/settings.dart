import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/providers/blockchain_listener.dart';
import 'package:gazelle/providers/theme_provider.dart';
import 'package:gazelle/providers/wallet_provider.dart';
import 'package:gazelle/screens/widgets/new_pin_code_dialog.dart';
import 'package:gazelle/screens/widgets/pin_code_dialog.dart';
import 'package:gazelle/services/wallet_service.dart';
import 'package:go_router/go_router.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsScreen extends ConsumerWidget {
  const SettingsScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => context.pop(),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _buildThemeSection(ref),
            _buildSecuritySection(context, ref),
            _buildNotificationSection(),
            _buildCurrencySection(),
            _buildLanguageSection(),
            _buildHelpSection(),
            _buildVersionSection(),
          ],
        ),
      ),
    );
  }

  Widget _buildThemeSection(WidgetRef ref) {
    final themeNotifier = ref.watch(themeProvider);

    return SwitchListTile(
      title: const Text('Dark Mode'),
      value: themeNotifier,
      onChanged: (value) {
        ref.read(themeProvider.notifier).toggleTheme();
      },
    );
  }

  Widget _buildSecuritySection(BuildContext context, WidgetRef ref) {
    return ExpansionTile(
      title: const Text('Security'),
      children: [
        ListTile(
          title: Padding(
            padding: const EdgeInsets.only(left: 8),
            child: const Text('Change PIN'),
          ),
          onTap: () async {
            final mnemonic =
                await Future.delayed(Duration.zero, () => showPinCodeDialog());
            if (mnemonic == null) return;

            final newPinCode = await showNewPinCodeDialog();
            if (newPinCode != null) {
              await WalletService.generateWallet(
                mnemonic: mnemonic,
                pinCode: newPinCode,
              );
            }
          },
        ),
        ListTile(
          title: Padding(
            padding: const EdgeInsets.only(left: 8),
            child: const Text('Erase Keystore'),
          ),
          onTap: () async {
            final confirm = await showDialog<bool>(
              context: context,
              builder: (context) => AlertDialog(
                title: const Text('Confirm Keystore Erasure'),
                content: const Text(
                    'Are you sure you want to erase the keystore? This action is irreversible and you will lose access to your wallet.'),
                actions: [
                  TextButton(
                    onPressed: () => context.pop(false),
                    child: const Text('Cancel'),
                  ),
                  TextButton(
                    onPressed: () => context.pop(true),
                    child: const Text('Erase',
                        style: TextStyle(color: Colors.red)),
                  ),
                ],
              ),
            );

            if (confirm == true) {
              ref
                  .read(blockchainListenerProvider(
                          ref.read(walletManagerProvider)!.address)
                      .notifier)
                  .stopBalanceListening();
              await ref.read(walletManagerProvider.notifier).clearWallet();

              // Navigate to the home page
              context.pop();
            }
          },
        ),
      ],
    );
  }

  Widget _buildNotificationSection() {
    return ExpansionTile(
      title: const Text('Notifications'),
      children: [
        SwitchListTile(
          title: Padding(
            padding: const EdgeInsets.only(left: 8),
            child: const Text('Enable Notifications'),
          ),
          value: false,
          onChanged: (value) {
            // TODOO: Implement enable notifications
          },
        ),
        ListTile(
          title: Padding(
            padding: const EdgeInsets.only(left: 8),
            child: const Text('Notification Sound'),
          ),
          onTap: () {
            // TODOO: Implement notification sound
          },
        ),
      ],
    );
  }

  Widget _buildCurrencySection() {
    return ListTile(
      title: const Text('Currency'),
      onTap: () {
        // TODOO: Implement currency selection
      },
    );
  }

  Widget _buildLanguageSection() {
    return ListTile(
      title: const Text('Language'),
      onTap: () {
        // TODOO: Implement language selection
      },
    );
  }

  Widget _buildHelpSection() {
    return ExpansionTile(
      title: const Text('Help & Support'),
      children: [
        ListTile(
          title: Padding(
            padding: const EdgeInsets.only(left: 8),
            child: const Text('Contact Us'),
          ),
          onTap: () async {
            const url = 'https://forum.duniter.org';
            if (await canLaunchUrl(Uri.parse(url))) {
              await launchUrl(Uri.parse(url));
            }
          },
        ),
      ],
    );
  }

  Widget _buildVersionSection() {
    return FutureBuilder<PackageInfo>(
      future: PackageInfo.fromPlatform(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListTile(
            title: const Text('Version'),
            subtitle:
                Text('${snapshot.data!.version}+${snapshot.data!.buildNumber}'),
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }
}
