import 'package:durt2/durt2.dart' show IdentitySuggestion;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/providers/blockchain_listener.dart';
import 'package:gazelle/providers/squid_provider.dart';
import 'package:gazelle/providers/wallet_provider.dart';
import 'package:gazelle/services/wallet_service.dart';
import 'package:gazelle/utils.dart';
import 'package:go_router/go_router.dart';
import 'package:gazelle/providers/transaction_provider.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class TransferDialog extends ConsumerStatefulWidget {
  @override
  TransferDialogState createState() => TransferDialogState();
}

class TransferDialogState extends ConsumerState<TransferDialog> {
  double amount = 0;
  bool canValidateAddress = false;
  bool canValidateAmount = false;

  final recerverController = TextEditingController();

  Future<void> onSubmitted() async {
    if (!canValidateAddress || !canValidateAmount) {
      return;
    }
    final keypair = await WalletService.getKeyPair();
    if (keypair == null) return;

    // final transactionService = TransactionService();
    final transactionService = ref.read(transactionManagerProvider.notifier);
    transactionService.pay(
        keypair: keypair, dest: recerverController.text, amount: amount);

    context.pop();
  }

  @override
  void dispose() {
    recerverController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardListener(
      focusNode: FocusNode(),
      onKeyEvent: (event) {
        if (event.logicalKey == LogicalKeyboardKey.tab) {
          if (event is KeyDownEvent) {
            if (HardwareKeyboard.instance.isShiftPressed) {
              FocusScope.of(context).previousFocus();
            } else {
              FocusScope.of(context).nextFocus();
            }
          }
        }
      },
      child: AlertDialog(
        title: const Text('Transfer'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TypeAheadField<IdentitySuggestion>(
              emptyBuilder: (context) =>
                  ListTile(title: Text('No identity found')),
              controller: recerverController,
              builder: (context, controller, focusNode) => TextField(
                controller: controller,
                focusNode: focusNode,
                autofocus: true,
                onSubmitted: (_) => onSubmitted(),
                onChanged: (value) {
                  canValidateAddress = isAddress(value);
                  setState(() {});
                },
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Receiver',
                ),
              ),
              suggestionsCallback: (pattern) {
                if (pattern.length >= 3 && !isAddress(pattern)) {
                  return ref.read(searchAddressByNameProvider(pattern).future);
                } else {
                  return null;
                }
              },
              itemBuilder: (context, suggestion) => ListTile(
                title: Text(suggestion.name),
                subtitle: Text(getShortAddress(suggestion.address)),
              ),
              onSelected: (suggestion) {
                recerverController.text = suggestion.address;
                canValidateAddress = true;
                setState(() {});

                Future.delayed(Duration.zero, () {
                  FocusScope.of(context).nextFocus();
                  FocusScope.of(context).nextFocus();
                });
              },
              debounceDuration: Duration(milliseconds: 300),
            ),
            const SizedBox(height: 16),
            TextField(
              onSubmitted: (_) => onSubmitted(),
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.deny(',', replacementString: '.'),
                FilteringTextInputFormatter.allow(
                  RegExp(r'(^\d+\.?\d{0,2})'),
                ),
              ],
              onChanged: (value) {
                final walletState = ref.watch(walletManagerProvider);
                final blockchainState =
                    ref.read(blockchainListenerProvider(walletState!.address));
                amount = double.parse(value.isEmpty ? '0' : value);
                canValidateAmount =
                    amount > 0 && amount < blockchainState.balance;
                setState(() {});
              },
              decoration: const InputDecoration(
                labelText: 'Amount',
                border: OutlineInputBorder(),
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () => context.pop(),
            child: const Text('Cancel'),
          ),
          ElevatedButton(
            onPressed: canValidateAddress && canValidateAmount
                ? () async {
                    onSubmitted();
                  }
                : null,
            child: const Text('Transfer'),
          ),
        ],
      ),
    );
  }
}
