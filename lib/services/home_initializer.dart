import 'package:durt2/durt2.dart' show Durt, Networks;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/providers/blockchain_listener.dart';
import 'package:gazelle/providers/wallet_provider.dart';

class HomeInitializer {
  HomeInitializer(this.ref);
  final WidgetRef ref;

  Future<void> init() async {
    // Initialize Durt
    await Durt().init(network: Networks.gdev);

    if (ref.read(walletManagerProvider.notifier).loadDefaultWallet()) {
      final walletData = ref.read(walletManagerProvider);
      ref
          .read(blockchainListenerProvider(walletData!.address).notifier)
          .startBalanceListening();
    }
  }
}
