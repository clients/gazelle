// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'squid_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$graphqlClientHash() => r'dca2f8aab58d85de2be93971139af63ea9ec07f9';

/// See also [graphqlClient].
@ProviderFor(graphqlClient)
final graphqlClientProvider = AutoDisposeProvider<GraphQLClient>.internal(
  graphqlClient,
  name: r'graphqlClientProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$graphqlClientHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef GraphqlClientRef = AutoDisposeProviderRef<GraphQLClient>;
String _$identityNameHash() => r'bcc5956bcaa909a032a30eab6d589d6edbbcfe76';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [identityName].
@ProviderFor(identityName)
const identityNameProvider = IdentityNameFamily();

/// See also [identityName].
class IdentityNameFamily extends Family<AsyncValue<String?>> {
  /// See also [identityName].
  const IdentityNameFamily();

  /// See also [identityName].
  IdentityNameProvider call(
    String address,
  ) {
    return IdentityNameProvider(
      address,
    );
  }

  @override
  IdentityNameProvider getProviderOverride(
    covariant IdentityNameProvider provider,
  ) {
    return call(
      provider.address,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'identityNameProvider';
}

/// See also [identityName].
class IdentityNameProvider extends AutoDisposeFutureProvider<String?> {
  /// See also [identityName].
  IdentityNameProvider(
    String address,
  ) : this._internal(
          (ref) => identityName(
            ref as IdentityNameRef,
            address,
          ),
          from: identityNameProvider,
          name: r'identityNameProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$identityNameHash,
          dependencies: IdentityNameFamily._dependencies,
          allTransitiveDependencies:
              IdentityNameFamily._allTransitiveDependencies,
          address: address,
        );

  IdentityNameProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.address,
  }) : super.internal();

  final String address;

  @override
  Override overrideWith(
    FutureOr<String?> Function(IdentityNameRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: IdentityNameProvider._internal(
        (ref) => create(ref as IdentityNameRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        address: address,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<String?> createElement() {
    return _IdentityNameProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is IdentityNameProvider && other.address == address;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, address.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin IdentityNameRef on AutoDisposeFutureProviderRef<String?> {
  /// The parameter `address` of this provider.
  String get address;
}

class _IdentityNameProviderElement
    extends AutoDisposeFutureProviderElement<String?> with IdentityNameRef {
  _IdentityNameProviderElement(super.provider);

  @override
  String get address => (origin as IdentityNameProvider).address;
}

String _$searchAddressByNameHash() =>
    r'3aa2d9f7528c431a420db0a7617d4e2504d61e2a';

/// See also [searchAddressByName].
@ProviderFor(searchAddressByName)
const searchAddressByNameProvider = SearchAddressByNameFamily();

/// See also [searchAddressByName].
class SearchAddressByNameFamily
    extends Family<AsyncValue<List<IdentitySuggestion>>> {
  /// See also [searchAddressByName].
  const SearchAddressByNameFamily();

  /// See also [searchAddressByName].
  SearchAddressByNameProvider call(
    String idtyName,
  ) {
    return SearchAddressByNameProvider(
      idtyName,
    );
  }

  @override
  SearchAddressByNameProvider getProviderOverride(
    covariant SearchAddressByNameProvider provider,
  ) {
    return call(
      provider.idtyName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'searchAddressByNameProvider';
}

/// See also [searchAddressByName].
class SearchAddressByNameProvider
    extends AutoDisposeFutureProvider<List<IdentitySuggestion>> {
  /// See also [searchAddressByName].
  SearchAddressByNameProvider(
    String idtyName,
  ) : this._internal(
          (ref) => searchAddressByName(
            ref as SearchAddressByNameRef,
            idtyName,
          ),
          from: searchAddressByNameProvider,
          name: r'searchAddressByNameProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$searchAddressByNameHash,
          dependencies: SearchAddressByNameFamily._dependencies,
          allTransitiveDependencies:
              SearchAddressByNameFamily._allTransitiveDependencies,
          idtyName: idtyName,
        );

  SearchAddressByNameProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.idtyName,
  }) : super.internal();

  final String idtyName;

  @override
  Override overrideWith(
    FutureOr<List<IdentitySuggestion>> Function(SearchAddressByNameRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: SearchAddressByNameProvider._internal(
        (ref) => create(ref as SearchAddressByNameRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        idtyName: idtyName,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<List<IdentitySuggestion>> createElement() {
    return _SearchAddressByNameProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SearchAddressByNameProvider && other.idtyName == idtyName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, idtyName.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin SearchAddressByNameRef
    on AutoDisposeFutureProviderRef<List<IdentitySuggestion>> {
  /// The parameter `idtyName` of this provider.
  String get idtyName;
}

class _SearchAddressByNameProviderElement
    extends AutoDisposeFutureProviderElement<List<IdentitySuggestion>>
    with SearchAddressByNameRef {
  _SearchAddressByNameProviderElement(super.provider);

  @override
  String get idtyName => (origin as SearchAddressByNameProvider).idtyName;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
