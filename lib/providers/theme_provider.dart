import 'package:hive/hive.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'theme_provider.g.dart';

@riverpod
class Theme extends _$Theme {
  final _box = Hive.box('settings');
  static const String _key = 'isDarkMode';

  @override
  bool build() {
    return _box.get(_key, defaultValue: false);
  }

  bool get isDarkMode => state;

  void toggleTheme() {
    _box.put(_key, !isDarkMode);
    state = !isDarkMode;
  }
}
