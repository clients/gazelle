// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wallet_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$walletManagerHash() => r'cfb9632693e2fc01d7202e60e9c09eba8fddcc8a';

/// See also [WalletManager].
@ProviderFor(WalletManager)
final walletManagerProvider =
    NotifierProvider<WalletManager, WalletData?>.internal(
  WalletManager.new,
  name: r'walletManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$walletManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$WalletManager = Notifier<WalletData?>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
