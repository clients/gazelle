// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$transactionManagerHash() =>
    r'696625efeef160387a0ee969655372d71141dd8c';

/// See also [TransactionManager].
@ProviderFor(TransactionManager)
final transactionManagerProvider = AutoDisposeNotifierProvider<
    TransactionManager, TransactionStatusModel>.internal(
  TransactionManager.new,
  name: r'transactionManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$transactionManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$TransactionManager = AutoDisposeNotifier<TransactionStatusModel>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
