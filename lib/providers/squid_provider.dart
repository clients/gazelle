import 'package:durt2/durt2.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'squid_provider.g.dart';

@riverpod
GraphQLClient graphqlClient(GraphqlClientRef ref) {
  return SquidService.client;
}

@riverpod
Future<String?> identityName(IdentityNameRef ref, String address) async =>
    await ref.read(graphqlClientProvider).getIdentityName(address);

@riverpod
Future<List<IdentitySuggestion>> searchAddressByName(
        SearchAddressByNameRef ref, String idtyName) async =>
    await ref.read(graphqlClientProvider).searchAddressByName(idtyName);
