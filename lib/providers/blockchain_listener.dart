import 'dart:async';
import 'package:durt2/durt2.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:ss58/ss58.dart';
import 'package:meta/meta.dart';

part 'blockchain_listener.g.dart';

@Riverpod(keepAlive: true)
class BlockchainListener extends _$BlockchainListener {
  late String address;
  @override
  BalanceState build(String address) {
    this.address = address;
    return BalanceState();
  }

  // Future<void> startBlockListening() async {
  //   final storageBlockKeyQuery = [Durt.instance.gdev.query.system.numberKey()];
  //   await Durt.instance.gdev.rpc.state.subscribeStorage(
  //     storageBlockKeyQuery,
  //     (storageChangeSet) async {
  //       state = state.copyWith(
  //         currentBlock: await Durt.instance.gdev.query.system.number(),
  //       );
  //     },
  //   );
  // }

  bool isListeningBalance() {
    return state.address == address;
  }

  Future<void> startBalanceListening() async {
    if (isListeningBalance()) return;

    final accountPubKey = Address.decode(address).pubkey;
    final accountInfoInit =
        await Durt.instance.gdev.query.system.account(accountPubKey);

    final storageAccountKeyQuery = [
      Durt.instance.gdev.query.system.accountKey(accountPubKey)
    ];
    final balanceStream = await Durt.instance.gdev.rpc.state.subscribeStorage(
      storageAccountKeyQuery,
      (storageChangeSet) async {
        final accountInfo =
            await Durt.instance.gdev.query.system.account(accountPubKey);
        state = state.copyWith(
          balance: accountInfo.data.free.toInt() / 100,
        );
      },
    );

    state = state.copyWith(
      address: address,
      balance: accountInfoInit.data.free.toInt() / 100,
      streamSubscription: balanceStream,
    );
  }

  Future<void> stopBalanceListening() async {
    state.streamSubscription?.cancel();
    state = state.copyWith(
      address: null,
      balance: null,
      streamSubscription: null,
    );
  }
}

@immutable
class BalanceState {
  final String address;
  final double balance;
  final StreamSubscription<StorageChangeSet>? streamSubscription;

  const BalanceState({
    this.address = '',
    this.balance = 0,
    this.streamSubscription,
  });

  BalanceState copyWith({
    String? address,
    double? balance,
    StreamSubscription<StorageChangeSet>? streamSubscription,
  }) {
    return BalanceState(
      address: address ?? this.address,
      balance: balance ?? this.balance,
      streamSubscription: streamSubscription ?? this.streamSubscription,
    );
  }
}
